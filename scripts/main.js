var app = angular.module('mainApp', ['VeForm', 'VeDates', 'ngMask']);

app.controller('mainController',function($scope, $http, $window, $sce, $timeout) {

	var templateName = 'sts-hs-select-fi';
	$scope.company = {};
	$scope.data = {};
	$scope.redirectSign = "https://www.sts.fi/";
	var template = null;

	$scope.busy = false;
	$scope.debug = true;

	$scope.lists = {};

	$http.get('assets/postnummer_se.json')
	.then(function(d) {
		$scope.lists.postnummerLength = 6;
		$scope.lists.postnummer = d.data;
	});

	$scope.recipient = {}
	$scope.signUrl = null;
	$scope.menus = {};
	$scope.lists = {};

	$scope.errors = {};

	$scope.data.lang = "fi_FI";

	$scope.translations = {
		"clear": {"en": "Clear", "nb": "Fjern", "sv": "Fjärn" },
		"close": {"en": "Close", "nb": "Lukk", "sv": "Stäng" },
		"today": {"en": "Today", "nb": "I dag", "sv": "I dag" },
	}
	$http.get("https://sheets.web-services.verified.eu/sts-hs-select-fi/translations").then(function (d) {
		 $scope.translations = $scope.data.translations = d.data; 
		 document.title = $scope.translations.templateTitle[$scope.data.lang];
		 
		$scope.token = $scope.translations.jwt[$scope.data.lang];
		$scope.namespace = $scope.translations.namespace[$scope.data.lang];	
		$scope.authenticate();


	});
	$http.get("https://sheets.web-services.verified.eu/sts-hs-select-fi/pdf-translations").then(function (d) {
		$scope.data.pdf_translations = d.data; 
	});

	$scope.authenticate = function () {
		console.log("inside authenticate");
		console.log("token:"  +$scope.token);
		console.log("namespace:"+ $scope.namespace);
        libvf.auth.authenticate({"token":  $scope.token, "namespace": $scope.namespace});
	};

	// VeLib.core.init({ descriptor_id:templateName })
	// .then( function ( ok ) {
	// 	return VeLib.public_templates.init();
	// } ).then( function ( status ) {
	// 	$scope.needsContextCreation = status.needsContextCreation;
	// 	return VeLib.public_templates.getTemplateInterface();
	// } ).then( function ( templates ) {
	// 	template = templates[ 0 ];
	// } );


	$scope.forms = {
		"firstForm": true,
		"secondForm": false,
		"thirdForm": false,
		"recipientForm": false
	};

	$scope.copyContact1 = function() {
		$scope.data.signer1_firstName = $scope.data.parent1_firstName;
		$scope.data.signer1_lastName = $scope.data.parent1_lastName;
		$scope.data.signer1_email = $scope.data.parent1_email;
	}

	$scope.copyContact2 = function() {
		$scope.data.signer2_firstName = $scope.data.parent2_firstName;
		$scope.data.signer2_lastName = $scope.data.parent2_lastName;
		$scope.data.signer2_email = $scope.data.parent2_email;
	}

	$scope.data.second_parent = {
		"1": true
	}
	var  getUrlParameter = function(param, dummyPath) {
        var sPageURL = dummyPath || window.location.search.substring(1),
            sURLVariables = sPageURL.split(/[&||?]/),
            res;
        for (var i = 0; i < sURLVariables.length; i += 1) {
            var paramName = sURLVariables[i],
                sParameterName = (paramName || '').split('=');
            if (sParameterName[0] === param) {
                res = sParameterName[1];
            }
        }
        return res;
	};
	$scope.data.country = getUrlParameter("country") || "fi";


	Date.prototype.yyyymmdd = function() {
	  var mm = this.getMonth() + 1; // getMonth() is zero-based
	  var dd = this.getDate();
		var yyyy = this.getFullYear();

		return yyyy + "-" + (mm>9 ? '' : '0') + mm + "-" + (dd>9 ? '' : '0') + dd;
	};

	// Hides all forms except the one chosen
	function displayForm(formName) {
		for (var form in $scope.forms)
		  $scope.forms[form] = false;

		$scope.forms[formName] = true;
	}

	$scope.displayForm = displayForm;

	$scope.noLookup = function() {
		displayForm("secondForm");
	}

	$scope.data.org_name = "Aktiebolaget Jönköpingsbostäder";
	$scope.data.org_nr = "556495-4716";
	$scope.data.org_giro = "545-6066";
	$scope.data.org_address = "c/o AB Jönköpingsbostäder, Box 325";
	$scope.data.org_zip = "551 15";
	$scope.data.org_town = "Jönköping";

	$scope.bisnodeLookup = function(orgNr) {

		$scope.busy = true;

		VeLib.bisnode.getPersonInfo(orgNr).then(function (data) {

			console.log(data);

			$scope.data.client_firstName = data.consumer.firstName;
			$scope.data.client_lastName = data.consumer.surname;
			$scope.data.client_ssn = $scope.data.orgNumber;
			$scope.data.client_address = data.consumer.nationalRegistrationAddressPostalAddressLine1;
			$scope.data.client_zip = data.consumer.nationalRegistrationAddressPostCode;
			$scope.data.client_town = data.consumer.nationalRegistrationAddressTown;

			displayForm("secondForm");

			$scope.busy = false;
			$scope.$digest();

		}).catch(function (err) {
			if (err.message == 'not found') {
				$scope.errors.orgNotFound = true;
				console.log(err);
			} else {
				$scope.errors.bisnodeException = true;
			}
			$scope.busy = false;
			$scope.$digest();
		});

	}

	$scope.tips = {};

	$scope.saveForm = function() {
		displayForm("thirdForm");
	}

	function prep() {

		var rightNow = new Date();
		var res = rightNow.toISOString().slice(0,10);

		// Must set these for flow to avoid filename NA_NA:
		$scope.data.fileName = "STS High School Contract " + $scope.data.firstName + " " + $scope.data.lastName + " " + res + ".pdf";
		$scope.data.customerName =  $scope.data.firstName + " " + $scope.data.lastName;
		$scope.data._isForwarded = true;

		// $scope.data.dob = $scope.data.dob.yyyymmdd();

	}

	$scope.generateRecipients = function() {
		$scope.recipients = [];
		var signingMethod = "email";

		if ($scope.data.country == "no") {
			signingMethod = "bankid-no";
		}else if ($scope.data.country == "sv") {
			signingMethod = "bankid-se";
		}else if ($scope.data.country == "fi") {
			signingMethod = "email";
		}

		$scope.recipients.push(
			{
				"givenName": $scope.data.signer1_firstName,
				"familyName": $scope.data.signer1_lastName,
				"language": "fi_FI",
				"signingMethod": signingMethod,
				"email": $scope.data.signer1_email,
				"order": 1,
				"role": {
					"action": "sign",
					"label": "Signer",
					"name": "signer"
				}
			}
		);

		if($scope.data.second_parent["1"]) {
			$scope.recipients.push(
				{
					"givenName": $scope.data.signer2_firstName,
					"familyName": $scope.data.signer2_lastName,
					"language": "fi_FI",
					"signingMethod": signingMethod,
					"email": $scope.data.signer2_email,
					"order": 3,
					"role": {
						"action": "sign",
						"label": "Signer",
						"name": "signer"
					}
				}
			);
		}

		$scope.recipients.push(
			{
				"givenName": $scope.data.firstName,
				"familyName": $scope.data.lastName,
				"language": "fi_FI",
				"signingMethod": signingMethod,
				"email": $scope.data.email,
				"order": 2,
				"role": {
					"action": "sign",
					"label": "Signer",
					"name": "signer"
				}
			}
		);

		return $scope.recipients;
	}

	Date.prototype.yyyymmdd = function() {
	  var mm = this.getMonth() + 1; // getMonth() is zero-based
	  var dd = this.getDate();
		var yyyy = this.getFullYear();

		return yyyy + "-" + (mm>9 ? '' : '0') + mm + "-" + (dd>9 ? '' : '0') + dd;
	};
	$scope.getHtml = function(el) {
		return $sce.trustAsHtml(el);
   };
	$scope.submitData = function() {

		if($scope.submitted) return;
		$scope.submitted = true;
		$scope.busy = true;
		prep();
		console.log(angular.toJson($scope.data));

		libvf.envelopes.create(templateName).then(function (res) {
			console.log("created envelope",res);
			envelope = res;
			envelopeId = res.id;
			return envelope.getBearerToken('/flows/simple-public-flow');
		}).then(function (token) {
			console.log("created bearer token",token);
			return libvf.auth.setToken(token);
		}).then(function () {
			return envelope.reflect();
		}).then(function () {
			return envelope.firstTemplate().setUserData($scope.data)
		}).then(function(){
			let recs = [];
			var recipientsData = $scope.generateRecipients();
			for(let res in recipientsData)
				recs.push(envelope.addRecipient(recipientsData[res]));
			return Promise.all(recs);
		}).then(function () {
			console.log("putting fileName",$scope.data.fileName);
			return envelope.firstDocument().put({ name: $scope.data.fileName });
		}).then(function () {
				return envelope.publish();
		}).then(function () {
			envelope.reflect();
			if($scope.data._isForwarded === 'false'){
				envelope.getSignToken("/flows/simple-public-flow")
				.then(function (token) {
					$scope.signUrl = "https://app.verified.eu/#/sign"+ envelope.data.uid + "?access_token=" + token + "&lang=" + $scope.data.lang + "&redirect_to=" + encodeURIComponent($scope.redirectSign);
					console.log("Got sign URL:", $scope.signUrl);
					window.location.href = $scope.signUrl;
					});
			}else{
				$scope.busy = false;
				$scope.published 	= true;
				$scope.$digest();
				$timeout(function() {
				 window.location.href = $scope.redirectSign 
				}, 5000)
			}
		}).catch(function (err) {
			console.log("ERROR", err);
			$scope.busy = false;
			$scope.error = err;
			$scope.$digest();
			throw err;
		});		
		// template.setData( JSON.parse(angular.toJson($scope.data)) );
		// $scope.busy = true;
		// VeLib.public_templates.submitFormData()
		// .then(function() {
		// 	return VeLib.public_templates.addRecipients($scope.generateRecipients());
		// })
		// .then( function () {
		// 	return VeLib.public_templates.publish();
		// } ).then( function ( url ) {
		// 	console.log( "got sign url", url );
		// 	$scope.signUrl ="https://app.verified.eu"+ url + '&lang=sv_SE&redirect_to=' + encodeURIComponent($scope.redirectSign);
		// 	$scope.busy = false;
		// 	$scope.published 	= true;
		// 	$scope.$digest();
		// 	$timeout(function() {
		// 	 window.location.href = $scope.redirectSign 
		// 	}, 5000)
		// } );

	}
});
